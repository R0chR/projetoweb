import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import SysRoutes from "./SysRoutes";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <Header />
        <SysRoutes />
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
