import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  getFirestore,
  collection,
  getDocs,
  query,
  where,
} from "firebase/firestore/lite";
import { auth, app } from "../../env";
import {
  H1,
  MainContainer,
  Container,
  Card,
  CardImg,
  CardH2,
  CardP,
  CardSpan,
  Button,
} from "./style/HomenageadosStyle";

const HomenageadosPage = () => {
  let [cardsData, setCardsData] = useState([]);
  const db = getFirestore(app);
  let { nameValue } = useParams();
  let [isAdmin, SetIsAdmin] = useState(false);
  let navigate = useNavigate();

  async function getPeople(db) {
    let collectionHomenageados;

    if (nameValue) {
      nameValue = nameValue.at(0).toUpperCase() + nameValue.slice(1);
      const spaceChar = nameValue.indexOf(" ");

      if (spaceChar !== -1 && spaceChar !== 0) {
        nameValue =
          nameValue.slice(0, spaceChar + 1) +
          nameValue.at(spaceChar + 1).toUpperCase() +
          nameValue.slice(spaceChar + 2);
      }

      const nextNameValue =
        nameValue.slice(0, -1) +
        String.fromCharCode(nameValue.charCodeAt(nameValue.length - 1) + 1);

      collectionHomenageados = query(
        collection(db, "homenageados"),
        where("name", ">=", nameValue),
        where("name", "<", nextNameValue)
      );
    } else {
      collectionHomenageados = collection(db, "homenageados");
    }

    const homenageadosDocs = await getDocs(collectionHomenageados);
    setCardsData(homenageadosDocs.docs.map((doc) => doc.data()));
  }

  async function getRole(db) {
    if (auth.currentUser && auth.currentUser.email) {
      const usuariosRef = collection(db, "usuarios");
      const q = query(
        usuariosRef,
        where("email", "==", auth.currentUser.email)
      );

      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        SetIsAdmin(doc.data().admin);
      });
    }
  }

  function sendToCadastrarHomenageado() {
    navigate("/cadastrar-homenageado");
  }

  useEffect(() => {
    getPeople(db);
    if (auth.currentUser) {
      getRole(db);
    }
  }, [app, db, isAdmin, nameValue]);

  return (
    <MainContainer>
      <div>
      <H1>Homenageados</H1>
      <div>
        {isAdmin && (
          <Container>
            <Button type="submit" onClick={sendToCadastrarHomenageado}>Adicionar Homenageado</Button>
          </Container>
        )}
        <Container>
          {cardsData.map((card, index) => (
            <Card key={index}>
              <CardImg src={card.imgSrc} alt={card.imgAlt} />
              <CardH2>{card.name}</CardH2>
              <CardP>{card.bio}</CardP>
              <CardSpan>Nasceu: {card.birthDate}</CardSpan>
              {card.deathDate ? (
                <CardSpan>Faleceu: {card.deathDate}</CardSpan>
              ) : (
                <CardSpan>⠀</CardSpan>
              )}
            </Card>
          ))}
        </Container>
      </div>
      </div>
    </MainContainer>
    
  );
};

export default HomenageadosPage;
