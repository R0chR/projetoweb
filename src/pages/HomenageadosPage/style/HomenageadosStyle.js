import styled from "styled-components";

export const H1 = styled.h1`
  text-align: center;
  margin-top: 1em;
`;

export const MainContainer = styled.div`
  background-color: #F0EDE5;
  margin: 0 auto;
  width: 100%;
  min-height: 88vh;
`;

export const Container = styled.div`
  background-color: #F0EDE5;
  display: flex;
  flex-wrap: wrap;
  justify-content: center !important;
  align-items: center;
  margin-bottom: 2em;

  @media screen and (max-width: 768px) {
    margin-bottom: 1em;
  }
`;

export const Card = styled.div`
  background-color: white;
  border-radius: 10px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  width: 300px;
  margin: 20px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
`;

export const CardImg = styled.img`
  width: 75%;
  height: 200px;
  object-fit: cover;
  margin-bottom: 20px;
`;

export const CardH2 = styled.h2`
  font-size: 20px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 10px;
`;

export const CardP = styled.p`
  font-size: 16px;
  text-align: center;
  margin-bottom: 20px;
  line-height: 1.5;
`;

export const CardSpan = styled.span`
  font-size: 14px;
  font-style: italic;
  text-align: right;
`;

export const Button = styled.button`
  cursor: pointer;
  padding: 10px 20px;
  background-color: #4caf50;
  border: none;
  border-radius: 5px;
  color: white;
  text-transform: uppercase;
  margin-top: 1em;

  &:hover {
    background-color: #45a049;
  }
`;