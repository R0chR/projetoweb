import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  min-height: 87vh;
  margin-right: 5em;
  margin-left: 5em;

  @media screen and (max-width: 1087px) {
    margin-right: 2em;
    margin-left: 2em;
    flex-direction: column;
    min-height: 70vh;
  }
`;

export const Article = styled.article`
  flex: 1;
  padding: 10px;
  flex-wrap: wrap;
  text-align: justify;
  margin-right: 2em;

  h1 {
    margin-top: 0;
    text-align: center;
  }

  p {
    line-height: 1.5;
  }

  @media screen and (max-width: 1087px) {
    text-align: justify;
    order: 2;
    margin-right: 0em;
  }
`;

export const Img = styled.div`
  img {
    display: flex;
    max-width: 100%;
    margin: auto 0;
    width: 100vw
    order: 2;
  }

  @media screen and (max-width: 1087px) {
    img {
      max-width: 100%;
      width: 60vw; // Adjust as needed
      margin-top: 1em;
      order: 1;
    }
  }
`;

export const P = styled.p`
  margin-top: 1em;
`;
