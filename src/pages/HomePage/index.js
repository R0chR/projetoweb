import React from "react";
import { MainContainer, Article, Img, P } from "./style/HomePageStyle";
import pinturaZumbi from "./../../assets/images/pintura-zumbi.jpg";
import { auth } from "../../env";

const HomePage = () => {
  return (
    <MainContainer>
      <Article>
        <h1>A Honraria Anual de Campinas Para a Comunidade Negra</h1>

        <P>
          O Diploma Zumbi dos Palmares, atribuído pela Câmara Municipal de
          Campinas, é um reconhecimento único e especial dedicado a indivíduos
          que têm impactado positivamente a comunidade negra local. Este prêmio,
          nomeado em homenagem a Zumbi dos Palmares, líder do Quilombo dos
          Palmares e um símbolo emblemático de resistência à escravidão no
          Brasil, é concedido todos os anos durante as celebrações do Dia da
          Consciência Negra.
        </P>
        <P>
          Desde a sua instituição em 1995, o Diploma Zumbi dos Palmares
          tornou-se uma das mais prestigiosas distinções na cidade de Campinas,
          ressaltando a importância e a influência da comunidade negra no
          desenvolvimento cultural, social e econômico da cidade. A premiação
          destaca as conquistas de indivíduos que se destacam não apenas pelos
          seus esforços pessoais, mas também pela sua dedicação à comunidade
          negra e ao avanço da igualdade racial.
        </P>
        <P>
          Os laureados vêm de todas as esferas da vida, desde artistas e
          acadêmicos a líderes comunitários e empresários. A diversidade dos
          homenageados reflete a rica tapeçaria da comunidade negra em Campinas
          e os muitos caminhos pelos quais os indivíduos podem contribuir para o
          seu desenvolvimento e prosperidade.
        </P>
        <P>
          O Diploma é mais do que um prêmio; é um sinal de
          gratidão e reconhecimento pelo trabalho incansável e pelos esforços
          daqueles que, dia após dia, contribuem para a construção de uma
          sociedade mais justa, equitativa e inclusiva. É também um lembrete do
          valor do trabalho coletivo e da importância da luta pela igualdade
          racial, ecoando o legado de Zumbi dos Palmares.
        </P>
        <P>
          Anualmente, a cerimônia de premiação é um evento altamente antecipado,
          cheio de celebração, reconhecimento e, mais importante, uma
          oportunidade para refletir sobre o progresso feito e o caminho que
          ainda temos a percorrer para alcançar a plena igualdade racial.
        </P>
      </Article>
      <Img>
        <img
          src={pinturaZumbi}
          alt="Pintura contendo imagem de Zumbi, último líder do Quilombo dos Palmares"
        />
      </Img>
    </MainContainer>
  );
};

export default HomePage;
