import {React, useState} from "react";
import { app } from "../../env";
import { getFirestore, doc, addDoc, collection} from "firebase/firestore/lite";
import {
  MainContainer,
  H1,
  CardForm,
  FormGroup,
  Label,
  Input,
  Textarea,
  SubmitButton,
} from "./style/ContatoPageStyle";

const ContatoPage = () => {
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [assunto, setAssunto] = useState("");
  const [mensagem, setMensagem] = useState("");

  const db = getFirestore(app);

  const handleSubmit = (event) => {
    event.preventDefault();

    const formValues = {
      nome: nome,
      email: email,
      assunto: assunto,
      mensagem: mensagem,
    };

    addToDB(formValues);
  };
  
  async function addToDB(formValues) {
    try{
    const coll = collection(db, "mensagens")
    await addDoc(coll, formValues)
    .then(() => {
      alert("Mensagem enviada com sucesso.");
    });
  } catch(e) {
    alert("Houve um erro. Tente novamente.");
    console.error("Erro => ", e);
  }
    
  }

  return (
    <MainContainer>
      <div>
        <H1>Formulário de Contato</H1>
        <CardForm>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <Label htmlFor="nome">Nome:</Label>
              <Input
                type="text"
                id="nome"
                name="nome"
                required
                value={nome}
                onChange={(e) => setNome(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="email">Email:</Label>
              <Input
                type="text"
                id="email"
                name="email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="assunto">Assunto:</Label>
              <Input
                type="text"
                id="assunto"
                name="assunto"
                required
                value={assunto}
                onChange={(e) => setAssunto(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="mensagem">Mensagem:</Label>
              <Textarea
                id="mensagem"
                name="mensagem"
                rows="5"
                cols="30"
                required
                value={mensagem}
                onChange={(e) => setMensagem(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <SubmitButton type="submit" value="Enviar" />
            </FormGroup>
          </form>
        </CardForm>
      </div>
    </MainContainer>
  );
};

export default ContatoPage;
