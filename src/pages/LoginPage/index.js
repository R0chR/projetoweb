import { React, useState } from "react";
import {
  MainContainer,
  H1,
  CardForm,
  FormGroup,
  Label,
  Input,
  P,
  PLink,
  SubmitButton,
} from "./style/LoginPageStyle";
import { NavLink, useNavigate } from "react-router-dom";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../env";

const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();

    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        alert("Erro ao fazer login. Tente novamente.");
        console.error("Erro => ", error);
      });
  };

  return (
    <MainContainer>
      <div>
        <H1>Login</H1>
        <CardForm>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <Label htmlFor="email">Email:</Label>
              <Input
                type="text"
                id="email"
                name="email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="password">Senha:</Label>
              <Input
                type="password"
                id="password"
                name="password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <SubmitButton type="submit" value="Enviar" />
            </FormGroup>
          </form>
          <P>Não possui uma conta?</P>
          <PLink>
            <NavLink className="menu-item" to="/registrar">
              Registre-se
            </NavLink>
          </PLink>
        </CardForm>
      </div>
    </MainContainer>
  );
};

export default LoginPage;
