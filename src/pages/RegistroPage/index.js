import { React, useState } from "react";
import {
  MainContainer,
  H1,
  CardForm,
  FormGroup,
  Label,
  Input,
  SubmitButton,
  P,
  PLink,
} from "./style/RegistroPageStyle";
import { NavLink, useNavigate } from "react-router-dom";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { getFirestore, doc, addDoc, collection } from "firebase/firestore/lite";
import { auth, app } from "../../env";

const RegistroPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const navigate = useNavigate();
  const db = getFirestore(app);

  const handleSubmit = (event) => {
    event.preventDefault();

    createUserWithEmailAndPassword(auth, email, password)
      .then(() => {
        alert("Usuário criado com sucesso.");
        const formValues = {
          email: auth.currentUser.email,
          name: name,
          isAdmin: false,
        };
        addToDB(formValues);
      })
      .catch((error) => {
        alert("Erro ao criar usuário. Tente novamente.");
        console.error("Erro => ", error);
      });
  };

  async function addToDB(formValues) {
    try {
      const coll = collection(db, "usuarios");
      await addDoc(coll, formValues).then(() => {
        navigate("/login");
      });
    } catch (e) {
      alert("Houve um erro. Tente novamente.");
      console.error("Erro => ", e);
    }
  }

  return (
    <MainContainer>
      <div>
        <H1>Registre-se</H1>
        <CardForm>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <Label htmlFor="name">Nome:</Label>
              <Input
                type="text"
                id="name"
                name="name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="email">Email:</Label>
              <Input
                type="text"
                id="email"
                name="email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="password">Senha:</Label>
              <Input
                type="password"
                id="password"
                name="password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <SubmitButton type="submit" value="Enviar" />
            </FormGroup>
          </form>
          <P>Já possui uma conta?</P>
          <PLink>
            <NavLink className="menu-item" to="/login">
              Faça o login
            </NavLink>
          </PLink>
        </CardForm>
      </div>
    </MainContainer>
  );
};

export default RegistroPage;
