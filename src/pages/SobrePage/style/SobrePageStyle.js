import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 87vh;

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 768px) {
    margin-bottom: 1em;
  }
`;

export const Article = styled.article`
  flex: 1;
  padding: 10px;
  flex-wrap: wrap;
  text-align: justify;
  order: 2;

  @media screen and (max-width: 768px) {
    text-align: justify;
    order: 2;
  }
`;

export const H1 = styled.h1`
  margin-top: 0;
  text-align: center;
  margin-bottom: 1em;

  @media screen and (max-width: 768px) {
    margin-top: 1em;
  }
`;

export const P = styled.p`
  line-height: 1.5;
  text-align: justify;
`;

export const Img = styled.div`
  display: flex;
  max-width: 80%;
  margin: auto 0;
  order: 2;

  @media screen and (max-width: 768px) {
    max-width: 100%;
    margin-top: 1em;
    order: 1;
  }
`;
