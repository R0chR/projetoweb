import React from "react";
import {
  MainContainer,
  Container,
  Article,
  H1,
  P,
  Img,
} from "./style/SobrePageStyle";

const SobrePage = () => {
  return (
    <MainContainer>
      <Container>
        <Article>
          <H1>Sobre o projeto</H1>
          <P>
            Projeto realizado por Leonardo de Vietro, Lucas Reuel e Gabriel
            Augusto para a disciplina de Desenvolvimento Web no Instituto
            Federal de São Paulo, Campinas - SP.
          </P>
        </Article>
      </Container>
    </MainContainer>
  );
};

export default SobrePage;
