import React, { useState, useEffect } from "react";
import {
  HeaderDiv,
  NavSearchContainer,
  LogoContainer,
  Logo,
  WebsiteName,
  HorizontalList,
  SearchContainer,
  Input,
  SubmitButton,
  Outer,
  Inner,
} from "./style/HeaderStyle";
import { NavLink, useNavigate } from "react-router-dom";
import logoZumbi from "./../../assets/images/logo-zumbi.jpg";
import searchIcon from "./../../assets/images/search-icon.png";
import { auth, app } from "../../env";
import { signOut } from "firebase/auth";

function Header() {
  const [inputValue, setInputValue] = useState("");
  const navigate = useNavigate();

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    navigate(`/homenageados/${inputValue}`);
  };
  
  return (
    <HeaderDiv>
      <LogoContainer>
        <Logo src={logoZumbi} />
        <WebsiteName>Diploma Zumbi dos Palmares</WebsiteName>
      </LogoContainer>
      <Outer>
        <Inner></Inner>
      </Outer>
      <NavSearchContainer>
        <nav>
          <HorizontalList>
            <li>
              <NavLink className="menu-item" to="/">
                Home
              </NavLink>
            </li>
            <li>
              <NavLink className="menu-item" to="/homenageados">
                Homenageados
              </NavLink>
            </li>
            <li>
              <NavLink className="menu-item" to="/contato">
                Contato
              </NavLink>
            </li>
            <li>
              <NavLink className="menu-item" to="/sobre">
                Sobre
              </NavLink>
            </li>
            <li>
              <NavLink className="menu-item" to="/login">
                Login
              </NavLink>
            </li>
          </HorizontalList>
        </nav>
        
          <form onSubmit={handleSubmit}>
          <SearchContainer>
            <Input
              type="text"
              placeholder="Pesquisar..."
              value={inputValue}
              onChange={handleInputChange}
            />
            <SubmitButton type="submit"><span><img src={searchIcon}></img></span></SubmitButton>
            </SearchContainer>
          </form>
          

        
      </NavSearchContainer>
    </HeaderDiv>
  );
}

export default Header;
