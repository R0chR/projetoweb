import styled from 'styled-components';

export const HeaderDiv = styled.header`
  background-color: black;
  color: white;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5em;
  text-align: center;
  height: 9.6vh;

  @media screen and (max-width: 1087px) {
    flex-direction: column;
    height: auto;
  }
`;

export const NavSearchContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-grow: 1;
  margin-left: 1em;

  @media screen and (max-width: 1087px) {
    flex-direction: column;
    margin-left: 0;
    align-items: center;
  }
`;

export const LogoContainer = styled.div`
  margin-left: 0.5em;
  margin-right: 1em;
  display: flex;
  align-items: center;

  @media screen and (max-width: 1087px) {
    margin-bottom: 1em;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`;

export const Logo = styled.img`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  margin-right: 10px;
`;

export const WebsiteName = styled.h1`
  font-family: "Karantina";
  font-style: normal;
  font-weight: 400;
  font-size: 32px;
  color: #ffffff;
  margin: 0px;
`;

export const HorizontalList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;

  li {
    display: inline-block;

    &:last-child {
      margin-right: 0;
    }
  }

  li .menu-item {
    display: inline;
    padding: 5px 10px;
    text-decoration: none;
    color: white;

    &:hover {
      border-radius: 6px;
      background-color: greenyellow;
      color: black;
      transition: ease-out 0.5s;
    }
  }

  @media screen and (max-width: 1087px) {
    li {
      display: block;
      margin-bottom: 0.5em;
    }
  }
`;

export const SearchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 1em;

  @media screen and (max-width: 1087px) {
    margin-top: 1em;
    margin-right: 0;
    align-self: center;
  }
`;

export const Input = styled.input`
  padding: 6px;
  border-radius: 5px;
  border: none;
  margin-right: 5px;
`;

export const SubmitButton = styled.button`
  padding: 5px;
  border-radius: 5px;
  border: none;
  background-color: white;
  color: black;
  cursor: pointer;

  &:hover {
      border-radius: 6px;
      background-color: lightgrey;
      color: black;
      transition: ease-in-out 0.1s;
    }
`;

export const Outer = styled.div`
  width: 1px;
  height: 100%;
  margin: auto;
  position: relative;
  overflow: hidden;
`;

export const Inner = styled.div`
  position: absolute;
  width: 100%;
  height: 40%;
  background: grey;
  top: 30%;
  box-shadow: 0px 0px 30px 20px grey;
`;
