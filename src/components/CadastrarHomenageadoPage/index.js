import { React, useState } from "react";
import { app } from "../../env";
import { getFirestore, addDoc, collection } from "firebase/firestore/lite";
import {
  MainContainer,
  H1,
  CardForm,
  FormGroup,
  Label,
  Input,
  Textarea,
  SubmitButton,
} from "./style/CadastrarHomenageadoPageStyle";
import { useNavigate } from "react-router-dom";

const CadastrarHomenageadoPage = () => {
  const [name, setName] = useState("");
  const [imgUrl, setImgUrl] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [deathDate, setDeathDate] = useState("");
  const [bio, setBio] = useState("");
  const navigate = useNavigate();

  const db = getFirestore(app);

  const handleSubmit = (event) => {
    event.preventDefault();
    const imgAlt = "Foto de " + name;

    if (deathDate === "" || deathDate === undefined) {
      const formValues = {
        name: name,
        imgSrc: imgUrl,
        imgAlt: imgAlt,
        birthDate: birthDate,
        bio: bio,
      };

      addToDB(formValues);
    } else {
      const formValues = {
        name: name,
        imgSrc: imgUrl,
        imgAlt: imgAlt,
        birthDate: birthDate,
        deathDate: deathDate,
        bio: bio,
      };

      addToDB(formValues);
    }
  };

  async function addToDB(formValues) {
    try {
      const coll = collection(db, "homenageados");
      await addDoc(coll, formValues).then(() => {
        alert("Homenageado adicionado com sucesso.");
        navigate("/homenageados");
      });
    } catch (e) {
      alert("Houve um erro. Tente novamente.");
      console.error("Erro => ", e);
    }
  }

  return (
    <MainContainer>
      <div>
        <H1>Cadastrar Homenageado</H1>
        <CardForm>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <Label htmlFor="name">Nome:</Label>
              <Input
                type="text"
                id="name"
                name="name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="imgUrl">URL da Foto:</Label>
              <Input
                type="text"
                id="imgUrl"
                name="imgUrl"
                required
                value={imgUrl}
                onChange={(e) => setImgUrl(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="birthDate">Nasceu:</Label>
              <Input
                type="text"
                id="birthDate"
                name="birthDate"
                required
                value={birthDate}
                onChange={(e) => setBirthDate(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="deathDate">Faleceu (Opcional):</Label>
              <Input
                type="text"
                id="deathDate"
                name="deathDate"
                value={deathDate}
                onChange={(e) => setDeathDate(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="bio">Curta biografia:</Label>
              <Textarea
                required
                maxLength={64}
                id="bio"
                name="bio"
                rows="5"
                cols="30"
                value={bio}
                onChange={(e) => setBio(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <SubmitButton type="submit" value="Enviar" />
            </FormGroup>
          </form>
        </CardForm>
      </div>
    </MainContainer>
  );
};

export default CadastrarHomenageadoPage;
