import styled from 'styled-components';

export const FooterDiv = styled.footer`
  background-color: black;
  color: white;
  padding: 0.3em;
  text-align: right;
  margin-top: auto;
  min-height: 3vh;
  height: 100%;
  width: auto;

  a {
    color: greenyellow;
  }

  @media screen and (max-width: 600px) {
    text-align: center;
  }
`;
