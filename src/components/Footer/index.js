import React from "react";
import { FooterDiv } from './style/FooterStyle';

const Footer = () => {
  return (
    <FooterDiv>
      <p>
        Desenvolvido por <a href="#">Leonardo De Vietro</a>,{" "}
        <a href="#">Lucas Reuel</a> e <a href="#">Gabriel Augusto</a> sob
        licença <a href="#">Los Pollos Hermanos</a>
      </p>
    </FooterDiv>
  );
};

export default Footer;
