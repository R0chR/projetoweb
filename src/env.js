// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCY0L0m5KAvTn2-pMZ_P5XKNw6gOgRUjFw",
  authDomain: "portal-zumbi-dos-palmares.firebaseapp.com",
  projectId: "portal-zumbi-dos-palmares",
  storageBucket: "portal-zumbi-dos-palmares.appspot.com",
  messagingSenderId: "670149407652",
  appId: "1:670149407652:web:3caec505438c33157b8731",
  measurementId: "G-CXX8WRYSTZ"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const auth = getAuth(app);