import React from "react";
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import HomenageadosPage from "./pages/HomenageadosPage";
import ContatoPage from "./pages/ContatoPage";
import SobrePage from "./pages/SobrePage";
import LoginPage from "./pages/LoginPage";
import RegistroPage from "./pages/RegistroPage";
import CadastrarHomenageadoPage from "./components/CadastrarHomenageadoPage";

const SysRoutes = () => {
  return (
    <Routes>
      <Route path="" element={<HomePage/>} />
      <Route exact path="homenageados/:nameValue?" element={<HomenageadosPage/>} />
      <Route path="contato" element={<ContatoPage/>} />
      <Route path="sobre" element={<SobrePage/>} />
      <Route path="login" element={<LoginPage/>} />
      <Route path="registrar" element={<RegistroPage/>} />
      <Route path="cadastrar-homenageado" element={<CadastrarHomenageadoPage/>}/>
    </Routes>
  );
};

export default SysRoutes;
